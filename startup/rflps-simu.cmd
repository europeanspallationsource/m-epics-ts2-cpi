< modPath

require(s7plc, 1.2.0)

epicsEnvSet("PREFIX", "$(RFLPS_PREFIX=LAB-011RFC)")

epicsEnvSet("PLCIP", "$(RFLPS_IP=10.4.3.66)")

#var s7plcDebug 5

## Datablocks
epicsEnvSet("TCPPORTCPU", "3000")
epicsEnvSet("PLCPORTCPU", "PLCCPU")
epicsEnvSet("INSIZECPU", "8")
epicsEnvSet("OUTSIZECPU", "4")

epicsEnvSet("TCPPORTAF", "3001")
epicsEnvSet("PLCPORTAF", "PLCAF")
epicsEnvSet("INSIZEAF", "660")
epicsEnvSet("OUTSIZEAF", "390")

epicsEnvSet("TCPPORTDIO", "3002")
epicsEnvSet("PLCPORTDIO", "PLCDIO")
epicsEnvSet("INSIZEDIO", "408")
epicsEnvSet("OUTSIZEDIO", "68")

epicsEnvSet("TCPPORTPSU", "3003")
epicsEnvSet("PLCPORTPSU", "PLCPSU")
epicsEnvSet("INSIZEPSU", "378")
epicsEnvSet("OUTSIZEPSU", "218")

s7plcConfigure("$(PLCPORTCPU)","$(PLCIP)",$(TCPPORTCPU),$(INSIZECPU),$(OUTSIZECPU),1,2500,500)
s7plcConfigure("$(PLCPORTAF)","$(PLCIP)",$(TCPPORTAF),$(INSIZEAF),$(OUTSIZEAF),1,2500,500)
s7plcConfigure("$(PLCPORTDIO)","$(PLCIP)",$(TCPPORTDIO),$(INSIZEDIO),$(OUTSIZEDIO),1,2500,500)
s7plcConfigure("$(PLCPORTPSU)","$(PLCIP)",$(TCPPORTPSU),$(INSIZEPSU),$(OUTSIZEPSU),1,2500,500)

#dbLoadRecords(rflpsCPU.db, "PREFIX=$(PREFIX), PLC=$(PLCPORTCPU), HOLDHIGH=1.0")
#dbLoadRecords(rflpsAF.db, "PREFIX=$(PREFIX), PLC=$(PLCPORTAF)")
#dbLoadRecords(rflpsDIO.db, "PREFIX=$(PREFIX), PLC=$(PLCPORTDIO)")
#dbLoadRecords(rflpsPSU.db, "PREFIX=$(PREFIX), PLC=$(PLCPORTPSU)")

dbLoadTemplate(../db/rflpsCPU.substitutions, "PREFIX=$(PREFIX)")
dbLoadTemplate(../db/rflpsAF.substitutions, "PREFIX=$(PREFIX)")
dbLoadTemplate(../db/rflpsDIO.substitutions, "PREFIX=$(PREFIX)")
dbLoadTemplate(../db/rflpsPSU.substitutions, "PREFIX=$(PREFIX)")

